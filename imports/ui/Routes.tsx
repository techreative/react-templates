import * as React from 'react'
import { Route, Switch } from 'react-router-dom'
import {withTracker} from 'meteor/react-meteor-data'

import Resolutions from './containers/Resolutions'
import { Layout } from './components/Layout'

import LoginUserWithToken from '../auth/LoginUserWithToken'
import { AuthenticatedRoute } from '../auth/AuthenticatedRoute'
import { Meteor } from 'meteor/meteor'



interface User {
  _id: string
}

interface Props {
  loading: boolean
  user: User
  client: any
}

const Routes: React.FC<Props> = ({ loading, user }) => {
  if (loading) return null

  const isUser = user && !!user._id

  return (
    <Switch>
      <Layout
        isUser={isUser}
        logOut={() => {
          Meteor.logout()
        }}
      >
        <AuthenticatedRoute isUser={isUser}>
          <Resolutions />
        </AuthenticatedRoute>
        <Route exact path="/login/:token" component={LoginUserWithToken} />
      </Layout>
    </Switch>
  )
}

export default withTracker(props=>{
  console.log(Meteor.userId())
  const loading = Meteor.userId() !== null && String(Meteor.userId()).length <= 0;
  const user = Meteor.user()
  console.log(loading);
  return {
    loading,
    user
  }
})(Routes)
