import * as React from 'react'
import gql from 'graphql-tag'
import { graphql, withApollo } from 'react-apollo'
import {withTracker} from 'meteor/react-meteor-data'
import ResolutionsCollection from '../../api/resolutions/resolutions'

import { compose } from 'recompose'

import { ResolutionForm } from '../components/Resolutions/ResolutionForm'
import { ResolutionItem } from '../components/Resolutions/ResolutionItem'

const Resolutions = ({ loading, resolutions }) => {
  if (loading) return null

  const submitForm = (value: string): void => {
    Meteor.call('createResolution',{
        name: value,
    })
  }

  const completeGoal = (_id: string, completed: boolean): void => {
    Meteor.call('completeResolution', {
        _id,
        completed,
    })
  }

  const deleteGoal = (_id: string): void => {
    Meteor.call('deleteResolution',{
        _id,
    })
  }

  return (
    <>
      <ResolutionForm onClick={submitForm} />
      {resolutions.map(({ name, _id, completed }) => (
        <ResolutionItem
          key={_id}
          _id={_id}
          name={name}
          completed={completed}
          onClick={completeGoal}
          onDelete={deleteGoal}
        />
      ))}
    </>
  )
}

export default withTracker(props=>{
  const loading = Meteor.subscribe('resolutions').ready() === false;
  const resolutions = loading ? [] : ResolutionsCollection.find().fetch()
  console.log(loading);
  return {
    loading,
    resolutions
  }
})(Resolutions);