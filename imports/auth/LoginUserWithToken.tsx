import * as React from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { LoginLinks } from 'meteor/loren:login-links'
import { withTracker } from 'meteor/react-meteor-data'

const LoginUserWithToken = () => {
  const { token } = useParams()
  const history = useHistory()

  if (!Meteor.userId()) {
    LoginLinks.loginWithToken(token, (e, r) => {
      history.push('/')
    })
  } else {
    history.push('/')
  }

  return <div />
}

export default withTracker(props=>{
  return {}
})(LoginUserWithToken)
