import { Meteor } from 'meteor/meteor';
import Resolutions from '../resolutions'

const isUser = (userId: string | undefined): void => {
  if (!userId) throw new Error('Unauthorized user')
}

Meteor.methods({
  createResolution({ name }) {
    isUser(this.userId)
    if (name.length === 0) throw new Error('Not long enough')

    const id = Resolutions.insert({
      name,
      completed: false,
      userId: this.userId,
    })
    return Resolutions.findOne(id)
  },
  completeResolution({ _id, completed }) {
    isUser(this.userId)

    const item =  Resolutions.findOne(_id)

    Resolutions.update(_id, {
      name: item.name,
      userId: item.userId,
      completed,
    })

    return Resolutions.findOne(_id)
  },
  updateResolution({ _id, name }) {
    isUser(this.userId)
    Resolutions.update(_id, {
      name,
    })
    return Resolutions.findOne(_id)
  },
  deleteResolution({ _id }) {
    isUser(this.userId)
    const oldResolution = Resolutions.findOne(_id)
    Resolutions.remove(_id)
    return oldResolution
  },
});