import {Meteor} from 'meteor/meteor';
import Resolutions from '../resolutions';

Meteor.publish('resolutions', function() {
      if (!this.userId) return []

      return Resolutions.find({
        userId: this.userId,
      });
});

Meteor.publish('resolution', function(  _id:string ) {
      if (!this.userId) return []
      return Resolutions.findOne(_id)
});

Meteor.publish('searchResolution', function({ key, value }) {
      if (!this.userId) return []
      return Resolutions.find({
        [key]: value,
        userId: this.userId,
      });
});